-- Insert Data into Code Tables
CREATE TEMPORARY TABLE tempCodes (
    code VARCHAR(10) NOT NULL,
    description VARCHAR(255) NOT NULL,
    codeType ENUM('', 'ItemLocation', 'ItemType', 'ItemCollection') NOT NULL,
    formatGroup ENUM('', 'Print', 'Media', 'Equipment', 'Internet'),
    formatSubgroup VARCHAR(20),
    categoryGroup ENUM('', 'Reference', 'Fiction', 'Nonfiction', 'Language'),
    categorySubgroup ENUM('', 'Large Print', 'Biography', 'Holiday', 'Picture', 'ESL')
);

LOAD DATA INFILE '/var/lib/mysql-files/20-Books/Integrated_Library_System__ILS__Data_Dictionary.csv'
IGNORE INTO TABLE tempCodes
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES;

-- not needed, can just set null
DELETE FROM tempCodes
WHERE code = 'unk';

-- duplicate entry
DELETE FROM tempCodes
WHERE code = 'spa' LIMIT 1;

INSERT IGNORE INTO LibraryCodes(code, description, codeType)
SELECT code, description, codeType
FROM tempCodes;

INSERT IGNORE INTO LocationCodes (code)
SELECT code
FROM tempCodes
WHERE codeType = 'ItemLocation';

INSERT IGNORE INTO TypeCodes (code, formatGroup, formatSubgroup, categoryGroup, categorySubgroup)
SELECT code, formatGroup, formatSubgroup, categoryGroup, categorySubgroup
FROM tempCodes
WHERE codeType = 'ItemType';

INSERT IGNORE INTO CollectionCodes (code, formatGroup, formatSubgroup, categoryGroup, categorySubgroup)
SELECT code, formatGroup, formatSubgroup, categoryGroup, categorySubgroup
FROM tempCodes
WHERE codeType = 'ItemCollection';

-- Insert Data into Library Table --
CREATE TEMPORARY TABLE tempItems (
    bibNumber INT, 
    title VARCHAR(2000),
    itemType VARCHAR(10),
    itemCollection VARCHAR(10),
    floating VARCHAR(10),
    itemLocation VARCHAR(10),
    itemCount INT
);

LOAD DATA INFILE '/var/lib/mysql-files/20-Books/Library_Collection_Inventory.csv'
IGNORE INTO TABLE tempItems
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(bibNumber, title,  @dummy, @dummy, @dummy,  @dummy, @dummy, itemType, itemCollection, floating, itemLocation,  @dummy, itemCount);

INSERT IGNORE INTO Items (bibNumber, title, itemCount)
SELECT bibNumber, SUBSTRING_INDEX(title, ' /', 1) as s, count(bibNumber)
FROM tempItems WHERE floating = 'NA' -- remove copies in other locations
GROUP BY bibNumber, title
ORDER BY s ASC;

CREATE TABLE tempInventory (
    bibNumber INT, 
    barcode VARCHAR(13),
    callNumber VARCHAR(100),
    CheckoutDate VARCHAR(40)
);

LOAD DATA INFILE '/var/lib/mysql-files/20-Books/Checkouts_By_Title_Data_Lens_2017.csv'
IGNORE INTO TABLE tempInventory
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(bibNumber, barcode, @dummy, @dummy, callNumber, checkoutDate);

INSERT IGNORE INTO Inventory (bibNumber, barcode)
SELECT bibNumber, barcode
FROM tempInventory INNER JOIN Items USING(bibNumber)
GROUP BY bibNumber, barcode;

-- Insert Data into Category Table --
CREATE TABLE tempCategory (
    bibNumber INT, 
    category VARCHAR(500)
);

LOAD DATA INFILE '/var/lib/mysql-files/Group14/Item_Categories.csv'
IGNORE INTO TABLE tempCategory
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(bibNumber, category);

INSERT IGNORE INTO ItemCategories (bibNumber, category)
SELECT bibNumber, category
FROM tempCategory INNER JOIN Items USING(bibNumber)
GROUP BY bibNumber, category;

-- Join Book Info to Library Items --
CREATE TABLE tempBooks (
    id INT,
    title VARCHAR(2000), 
    ratingDist1 VARCHAR(255),
    pageCount INT,
    ratingDist4 VARCHAR(255),
    totalRatings VARCHAR(255),
    publishMonth VARCHAR(2),
    publishDay VARCHAR(2),
    publisher VARCHAR(255),
    reviewCount INT,
    publishYear VARCHAR(4),
    language VARCHAR(20),
    author VARCHAR(255),
    averageRating FLOAT(3,2),
    ratingDist2 VARCHAR(255),
    ratingDist5 VARCHAR(255),
    ISBN VARCHAR(13),
    ratingDist3 VARCHAR(255)
);

LOAD DATA INFILE '/var/lib/mysql-files/20-Books/book1-100k.csv'
IGNORE INTO TABLE tempBooks
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES;

CREATE TEMPORARY TABLE tempISBN (
    bibNumber INT,
    ISBN VARCHAR(13)
);

LOAD DATA INFILE '/var/lib/mysql-files/Group14/Books_ISBN.csv'
IGNORE INTO TABLE tempISBN
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES;

INSERT IGNORE INTO Books (bibNumber, pageCount, language, author, publisher, publishedDate, ratingDist1, ratingDist2, ratingDist3, ratingDist4, ratingDist5, totalRatings, averageRating, reviewCount, title)
SELECT bibNumber, pageCount, language, author, publisher, CONCAT(publishYear, "-", publishDay, "-", publishMonth), ratingDist1, ratingDist2, ratingDist3, ratingDist4, ratingDist5, totalRatings, averageRating, reviewCount, tempBooks.title
FROM (tempBooks INNER JOIN tempISBN USING(ISBN) INNER JOIN Items USING(bibNumber))
GROUP BY bibNumber, pageCount, language, author, publisher, publishYear, publishMonth, publishDay, ratingDist1, ratingDist2, ratingDist3, ratingDist4, ratingDist5, totalRatings, averageRating, reviewCount, tempBooks.title;

INSERT IGNORE INTO ISBN (bookID, isbn)
SELECT bookID, ISBN
FROM (tempISBN INNER JOIN Books USING(bibNumber))
GROUP BY bookID, ISBN;

INSERT IGNORE INTO Publishers (name)
SELECT DISTINCT publisher
FROM Books
WHERE publisher != '';

INSERT IGNORE INTO Authors (name)
SELECT DISTINCT author
FROM Books
WHERE author != '';

INSERT IGNORE INTO BookAuthors (bookID, authorID)
SELECT bookID, authorID
FROM Books INNER JOIN Authors ON Books.author = Authors.name;

INSERT IGNORE INTO Publications (bookID, publisherID, publishedDate)
SELECT bookID, publisherID, publishedDate
FROM Books INNER JOIN Publishers ON Books.publisher = Publishers.name;

INSERT IGNORE INTO BookRatings (bookID, ratingDist1, ratingDist2, ratingDist3, ratingDist4, ratingDist5, averageRating, totalRatings, reviewCount)
SELECT bookID, 
SUBSTRING_INDEX(ratingDist1, '1:', -1),
SUBSTRING_INDEX(ratingDist2, '2:', -1),
SUBSTRING_INDEX(ratingDist3, '3:', -1),
SUBSTRING_INDEX(ratingDist4, '4:', -1),
SUBSTRING_INDEX(ratingDist5, '5:', -1),
averageRating, 
SUBSTRING_INDEX(totalRatings, 'total:', -1),
reviewCount
FROM Books;

-- Generate Users and Review --
CREATE TEMPORARY TABLE tempReviews (
    userID INT,
    firstName VARCHAR(30),
    lastName VARCHAR(30),
    name VARCHAR(255),
    rating INT
);

LOAD DATA INFILE '/var/lib/mysql-files/Group14/User_Reviews.csv'
IGNORE INTO TABLE tempReviews
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n';

INSERT IGNORE INTO Users (userID, firstName, lastName)
SELECT userID, firstName, lastName
FROM tempReviews;

INSERT IGNORE INTO UserReviews (userID, bookID, rating)
SELECT userID, bookID, rating
FROM tempReviews INNER JOIN Books ON tempReviews.name = Books.title;

-- Load checkout records --
CREATE TEMPORARY TABLE tempCheckouts (
    bibNumber INT,
    barcode VARCHAR(13),
    userID INT,
    checkoutDate DATE,
    dueDate DATE,
    returnedDate DATE
);

LOAD DATA INFILE '/var/lib/mysql-files/Group14/User_Checkouts_2017.csv'
IGNORE INTO TABLE tempCheckouts
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n';

INSERT INTO CheckoutRecords (bibNumber, barcode, userID, CheckoutDate, dueDate, returnedDate)
SELECT bibNumber, barcode, userID, CheckoutDate, dueDate, returnedDate
FROM (tempCheckouts INNER JOIN Inventory USING(bibNumber, barcode) INNER JOIN Users USING(userID))
GROUP BY bibNumber, barcode, userID, CheckoutDate, dueDate, returnedDate;

ALTER TABLE CheckoutRecords
ADD PRIMARY KEY (bibNumber, barcode, userID, checkoutDate);

ALTER TABLE CheckoutRecords
ADD FOREIGN KEY (bibNumber, barcode) REFERENCES Inventory(bibNumber, barcode);

ALTER TABLE CheckoutRecords
ADD FOREIGN KEY (userID) REFERENCES Users(userID);

-- Connect items to library --
INSERT IGNORE INTO ItemCodes (bibNumber, code)
SELECT bibNumber, itemType
FROM tempItems INNER JOIN Items USING(bibNumber)
GROUP BY bibNumber, itemType;

INSERT IGNORE INTO ItemCodes (bibNumber, code)
SELECT bibNumber, itemLocation
FROM tempItems INNER JOIN Items USING(bibNumber)
GROUP BY bibNumber, itemLocation;

INSERT IGNORE INTO ItemCodes (bibNumber, code)
SELECT bibNumber, ItemCollection
FROM tempItems INNER JOIN Items USING(bibNumber)
GROUP BY bibNumber, ItemCollection;

-- delete extracted book columns --
ALTER TABLE Books
DROP COLUMN title,
DROP COLUMN author,
DROP COLUMN publisher,
DROP COLUMN publishedDate,
DROP COLUMN ratingDist1,
DROP COLUMN ratingDist2,
DROP COLUMN ratingDist3,
DROP COLUMN ratingDist4,
DROP COLUMN ratingDist5,
DROP COLUMN averageRating,
DROP COLUMN totalRatings,
DROP COLUMN reviewCount;

-- Drop all temp tables --
DROP TABLE tempCodes;
DROP TABLE tempItems;
DROP TABLE tempInventory;
DROP TABLE tempCategory;
DROP TABLE tempBooks;
DROP TABLE tempISBN;
DROP TABLE tempReviews;
DROP TABLE tempCheckouts;
