-- Code Tables --
CREATE TABLE LibraryCodes (
    code VARCHAR(10) NOT NULL, -- check longest length
    description VARCHAR(255) NOT NULL,
    codeType ENUM('', 'ItemLocation', 'ItemType', 'ItemCollection') NOT NULL,
    PRIMARY KEY (code)
);

CREATE TABLE LocationCodes ( -- Is this even needed?
    code VARCHAR(10) NOT NULL,
    PRIMARY KEY (code),
    FOREIGN KEY (code) REFERENCES LibraryCodes(code)
);

CREATE TABLE TypeCodes (
    code VARCHAR(10) NOT NULL,
    formatGroup ENUM('', 'Print', 'Media', 'Equipment', 'Internet'),
    formatSubgroup VARCHAR(20), -- consider making enum too
    categoryGroup ENUM('', 'Reference', 'Fiction', 'Nonfiction', 'Language'),
    categorySubgroup ENUM('', 'Large Print', 'Biography', 'Holiday', 'Picture', 'ESL'),
    PRIMARY KEY (code),
    FOREIGN KEY (code) REFERENCES LibraryCodes(code)
);

CREATE TABLE CollectionCodes (
    code VARCHAR(10) NOT NULL,
    formatGroup ENUM('', 'Print', 'Media', 'Equipment', 'Internet'),
    formatSubgroup VARCHAR(20),
    categoryGroup ENUM('', 'Reference', 'Fiction', 'Nonfiction', 'Language'),
    categorySubgroup ENUM('', 'Large Print', 'Biography', 'Holiday', 'Picture', 'ESL'),
    PRIMARY KEY (code),
    FOREIGN KEY (code) REFERENCES LibraryCodes(code)
);

-- Library Tables --
CREATE TABLE Items (
    bibNumber INT NOT NULL, 
    title VARCHAR(2000) NOT NULL DEFAULT '',
    itemCount INT NOT NULL,
    PRIMARY KEY (bibNumber)
);

CREATE TABLE Inventory (
    bibNumber INT NOT NULL,
    barcode VARCHAR(13), -- discriminator
    PRIMARY KEY (bibNumber, barcode),
    FOREIGN KEY (bibNumber) REFERENCES Items(bibNumber)
);

-- Book Info Tables --
CREATE TABLE Books (
    bookID INT NOT NULL AUTO_INCREMENT,
    bibNumber INT,
    pageCount int,
    language VARCHAR(255),
    title VARCHAR(2000), -- temp
    author VARCHAR(255), -- temp
    publisher VARCHAR(255), -- temp
    publishedDate DATE, -- temp
    ratingDist1 VARCHAR(255), -- temp
    ratingDist2 VARCHAR(255), -- temp
    ratingDist3 VARCHAR(255), -- temp
    ratingDist4 VARCHAR(255), -- temp
    ratingDist5 VARCHAR(255), -- temp
    averageRating FLOAT(3,2), -- temp
    totalRatings VARCHAR(255), -- temp
    reviewCount INT, -- temp
    PRIMARY KEY (bookID),
    FOREIGN KEY (bibNumber) REFERENCES Items(bibNumber)
);

CREATE TABLE ISBN (
    bookID INT NOT NULL,
    isbn VARCHAR(13), -- old has 10 digits, new has 13
    PRIMARY KEY (bookID, ISBN),
    FOREIGN KEY (bookID) REFERENCES Books(bookID)
);

CREATE TABLE Publishers (
    publisherID INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    PRIMARY KEY (publisherID)
);

CREATE TABLE Authors (
    authorID INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(255),
    PRIMARY KEY (authorID)
);

CREATE TABLE BookRatings (
    bookID INT NOT NULL,
    ratingDist1 INT NOT NULL DEFAULT 0,
    ratingDist2 INT NOT NULL DEFAULT 0,
    ratingDist3 INT NOT NULL DEFAULT 0,
    ratingDist4 INT NOT NULL DEFAULT 0,
    ratingDist5 INT NOT NULL DEFAULT 0,
    averageRating FLOAT(3,2),
    totalRatings INT NOT NULL DEFAULT 0,
    reviewCount INT NOT NULL DEFAULT 0,
    PRIMARY KEY (bookID),
    FOREIGN KEY (bookID) REFERENCES Books(bookID)
);

CREATE TABLE Users (
    userID INT NOT NULL AUTO_INCREMENT,
    firstName VARCHAR(255),
    lastName VARCHAR(255),
    PRIMARY KEY (userID)
);

CREATE TABLE UserReviews (
    userID INT NOT NULL,
    bookID INT NOT NULL,
    rating INT NOT NULL,
    PRIMARY KEY (userID, bookID),
    FOREIGN KEY (userID) REFERENCES Users(userID),
    FOREIGN KEY (bookID) REFERENCES Books(bookID)
);

-- Relationship Tables --
CREATE TABLE ItemCategories (
    bibNumber INT NOT NULL,
    category VARCHAR(255) NOT NULL,
    PRIMARY KEY (bibNumber, category),
    FOREIGN KEY (bibNumber) REFERENCES Items(bibNumber)
);

CREATE TABLE Publications (
    bookID INT NOT NULL,
    publisherID INT NOT NULL,
    publishedDate DATE,
    PRIMARY KEY (bookID, publisherID),
    FOREIGN KEY (bookID) REFERENCES Books(bookID),
    FOREIGN KEY (publisherID) REFERENCES Publishers(publisherID)
);

CREATE TABLE BookAuthors (
    bookID INT NOT NULL,
    authorID INT NOT NULL,
    PRIMARY KEY (bookID, authorID),
    FOREIGN KEY (bookID) REFERENCES Books(bookID),
    FOREIGN KEY (authorID) REFERENCES Authors(authorID)
);

CREATE TABLE CheckoutRecords (
    bibNumber INT NOT NULL,
    barcode VARCHAR(13) NOT NULL,
    userID INT NOT NULL,
    checkoutDate DATETIME NOT NULL,
    dueDate DATETIME NOT NULL,
    returnedDate DATETIME
);

CREATE TABLE ItemCodes (
    bibNumber INT NOT NULL,
    code VARCHAR(10) NOT NULL,
    PRIMARY KEY (bibNumber, code),
    FOREIGN KEY (bibNumber) REFERENCES Items(bibNumber),
    FOREIGN KEY (code) REFERENCES LibraryCodes(code)
);
