import csv
from os import close

with open('datasets/Item_Categories.csv', 'w', newline='\n', encoding='utf-8') as csvfile:
    writer = csv.writer(csvfile, lineterminator='\n',
                        quotechar='"', quoting=csv.QUOTE_ALL)
    f = open('datasets/Library_Collection_Inventory.csv')
    c = csv.reader(f)

    for i, row in enumerate(c):
        if i == 0:
            continue
        bib = row[0]
        if not len(row[6]):
            continue
        categories = row[6].split(', ')
        for category in categories:
            writer.writerow([bib, category])

    f.close()
