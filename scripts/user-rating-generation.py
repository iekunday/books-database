import csv
import string
import random

with open('datasets/User_Reviews.csv', 'w', newline='\n', encoding='utf-8') as csvfile:
    writer = csv.writer(csvfile, lineterminator='\n',
                        quotechar='"', quoting=csv.QUOTE_ALL)
    f = open('datasets/user_rating_0_to_1000.csv')
    c = csv.reader(f)

    prev = -1
    letters = string.ascii_lowercase
    firstName = lastName = ''
    l = 0
    for i, row in enumerate(c):
        if i == 0:
            continue
        if prev != row[0]:
            l = random.randint(3, 10)
            firstName = ''.join(random.choice(letters) for _ in range(l))
            lastName = ''.join(random.choice(letters) for _ in range(l))
        rating = row[2]
        if rating == 'did not like it':
            rating = 1
        elif rating == 'it was ok':
            rating = 2
        elif rating == 'liked it':
            rating = 3
        elif rating == 'really liked it':
            rating = 4
        elif rating == 'it was amazing':
            rating = 5
        else:
            rating = 0
        writer.writerow([row[0], firstName, lastName, row[1], rating])
        prev = row[0]
