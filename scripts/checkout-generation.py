import csv
import random
import datetime

with open('datasets/User_Checkouts_2017.csv', 'w', newline='\n', encoding='utf-8') as csvfile:
    writer = csv.writer(csvfile, lineterminator='\n',
                        quotechar='"', quoting=csv.QUOTE_ALL)
    f = open('datasets/Checkouts_By_Title_Data_Lens_2017.csv')
    c = csv.reader(f)

    for i, row in enumerate(c):
        if i == 0:
            continue
        id = random.randint(1, 1000)
        s = row[5].split(" ")
        checkout = s[0]
        s1 = s[0].split("/")
        s2 = [s1[2], s1[0], s1[1]]
        checkout_f = '-'.join(s2) + ' ' + s[1]
        t = datetime.datetime.strptime(checkout, '%m/%d/%Y')
        delay = datetime.timedelta(days=14)
        due = t + delay

        writer.writerow([row[0], row[1], id, checkout_f, due,  due])

    f.close()
