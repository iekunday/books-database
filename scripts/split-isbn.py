import csv
from os import close

with open('datasets/Books_ISBN.csv', 'w', newline='\n', encoding='utf-8') as csvfile:
    writer = csv.writer(csvfile, lineterminator='\n',
                        quotechar='"', quoting=csv.QUOTE_ALL)
    f = open('datasets/Library_Collection_Inventory.csv')
    c = csv.reader(f)

    for i, row in enumerate(c):
        if i == 0:
            continue
        bib = row[0]
        if not len(row[3]):
            continue
        categories = row[3].split(', ')
        for category in categories:
            writer.writerow([bib, category])

    f.close()
