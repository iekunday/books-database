import mysql.connector
import check

options = [
    ['CANCEL'],
    ['SEARCH'],
    ['CLEAR SEARCH FIELDS'],
    ['Title contains ', '*'],
    ['Category contains ', '*'],
    ['Author name contains ', '*'],
    ['Publisher name contains ', '*'],
    ['Published date between ', '*', ' and ', '*'],
    ['Language ', '*'],
    ['Average Rating between ', '*', ' and ', '*']
    ]


def searchAuthors(database, cursor):
  
    sql = ("CREATE TEMPORARY TABLE tempAuthors"
        + " AS (SELECT authorID, name AS authorName FROM Authors WHERE name LIKE '%" + options[5][1] + "%')")
    cursor.execute(sql)


def searchPublishers(database, cursor):

    sql = ("CREATE TEMPORARY TABLE tempPublishers"
        + " AS (SELECT publisherID, name AS publisherName FROM Publishers WHERE name LIKE '%" + options[6][1] + "%')")
    cursor.execute(sql)
    
# Join multiple tables based on what fields were changed. 


def search(database, cursor, book):
    
    join = ""

    # Apply book-only queries
    if (book):
        # GET AUTHORS THAT MATCH SEARCH
        if (not options[5][1] == "*"):
            searchAuthors(database, cursor)
            sql = ("CREATE TEMPORARY TABLE tempBookAuthors"
                   + " AS (SELECT * FROM BookAuthors INNER JOIN tempAuthors USING(authorID))")
            cursor.execute(sql)
        else:
            sql = ("CREATE TEMPORARY TABLE tempAuthors"
                    + " AS (SELECT authorID, name AS authorName FROM Authors)")
            cursor.execute(sql)
            sql = "CREATE TEMPORARY TABLE tempBookAuthors AS (SELECT * FROM BookAuthors INNER JOIN tempAuthors USING(authorID))"
            cursor.execute(sql)

        # GET PUBLISHERS AND PUBLICATION DATES THAT MATCH SEARCH
        where = ""
        if (not options[7][1] == "*"):
            where = (" WHERE YEAR(publishedDate) >= " + options[7][1]
                    + " AND YEAR(publishedDate) <= " + options[7][3])
        if (not options[6][1] == "*"):
            searchPublishers(database, cursor)
            sql = ("CREATE TEMPORARY TABLE tempPublications"
                    + " AS (SELECT * FROM Publications INNER JOIN tempPublishers USING(publisherID)" + where + ")")
            cursor.execute(sql)
        else:
            sql = ("CREATE TEMPORARY TABLE tempPublishers"
                    + " AS (SELECT publisherID, name AS publisherName FROM Publishers)")
            cursor.execute(sql)
            sql = "CREATE TEMPORARY TABLE tempPublications AS (SELECT * FROM Publications INNER JOIN tempPublishers USING(publisherID)" + where + ")"
            cursor.execute(sql)
            

        # GET CONDITIONS FOR RATINGS AND LANGUAGE
        where = ""
        if (not options[9][1] == "*"):
            where = (" WHERE averageRating >= " + options[9][1]
                     + " AND averageRating <= " + options[9][3])
        if (not options[8][1] == "*"):
            if (where == ""):
                where = " WHERE"
            else:
                where = where + " AND"
            where = where + " language LIKE '%" + options[8][1] + "%'"
                

        # JOIN ALL OF THE BOOK CRITERIA TOGETHER
        sql = ("CREATE TEMPORARY TABLE tempBooks AS"
               + " (SELECT * FROM Books INNER JOIN tempBookAuthors USING(bookID)"
               + " INNER JOIN tempPublications USING(bookID)"
               + " INNER JOIN BookRatings USING(bookID)"
               + where + ")")
        cursor.execute(sql)

        join = " INNER JOIN tempBooks USING(bibNumber)"


    where = ""
    if (not options[3][1] == "*"):
        where = " WHERE title LIKE '%" + options[3][1] + "%'"

    if (not options[4][1] == "*"):
        if (where == ""):
            where = " WHERE"
        else:
            where = where + " AND"
        where = where + " category LIKE '%" + options[4][1] + "%'"

    # JOIN WITH ITEMS TO PERFORM THE SEARCH
    sql = ("SELECT DISTINCT bibNumber, title, category, formatSubgroup"
                    + " FROM Items INNER JOIN ItemCategories USING(bibNumber)"
                    + " INNER JOIN ItemCodes USING(bibNumber)"
                    + " INNER JOIN CollectionCodes USING(code)" + join + where)
    if (book):
        sql = ("SELECT DISTINCT bibNumber, title, authorName, publisherName, year(publishedDate), language, category, formatSubgroup, averageRating"
                    + " FROM Items INNER JOIN ItemCategories USING(bibNumber)"
                    + " INNER JOIN ItemCodes USING(bibNumber)"
                    + " INNER JOIN CollectionCodes USING(code)" + join + where)
    cursor.execute(sql)
    k = 0
    results = cursor.fetchall()
    for result in results:
        if (not book):
            print(str(k) + " : \"" + 
                result[1] + "\". Category: " + result[2] + ". Format: " + result[3])
        else:
            print(str(k) + " : \"" + result[1] + "\" by " + result[2] + ". Format: " + result[7] + ". Published by: " + 
                result[3] + " (" + str(result[4]) + ") in " + result[5] + ". Category: " + result[6] + ". Rating = " + str(result[8]))
        k = k + 1

    # CLEAN UP TABLES FOR NEXT SEARCH
    sql = "DROP TEMPORARY TABLE IF EXISTS tempAuthors"
    cursor.execute(sql)
    sql = "DROP TEMPORARY TABLE IF EXISTS tempBookAuthors"
    cursor.execute(sql)
    sql = "DROP TEMPORARY TABLE IF EXISTS tempPublishers"
    cursor.execute(sql)
    sql = "DROP TEMPORARY TABLE IF EXISTS tempPublications"
    cursor.execute(sql)
    sql = "DROP TEMPORARY TABLE IF EXISTS tempBooks"
    cursor.execute(sql)
    sql = "DROP TEMPORARY TABLE IF EXISTS tempCodes"
    cursor.execute(sql)

    bibNumber = -1
    selection = check.yesNo("Do you see what you are looking for? Y/N: ")
    if (selection.lower() == 'y'):
        selection = check.intInput("\nPlease enter your selection #: ", 0, k - 1, True)
        bibNumber = results[selection][0]
        print(bibNumber)

    return bibNumber


# The main user interface for the module
def menu(database, cursor, forceBooks):

    bibNumber = -1
    book = False

    for option in options:
        if (len(option) > 1):
            option[1] = "*"
            if (len(option) > 2):
                option[3] = "*"
    
    while (True):

        i = 0
        for option in options:
            string = ""
            for segment in option:
                string = string + segment
            print(str(i) + " : " + string)
            i = i + 1

        selection = check.intInput(
            "\nPlease select the # of the parameter you want to change, or the # for Search, Cancel, or Clear: ", 0, 9, True)
        
        if (selection == 0):
            return -1

        if (selection == 1):
            bibNumber = search(database, cursor, book)
            if (bibNumber >= 0):
                break
        
        elif (selection == 2):
            for option in options:
                if (len(option) > 1):
                    option[1] = "*"
                    if (len(option) > 2):
                        option[3] = "*"
            
        elif (selection == 3):
            options[selection][1] = input("Please enter a part of the title: ")
            
        elif (selection == 4):
            options[selection][1] = input(
                "Please enter a part of the category: ")
            
        elif (selection == 5):
            options[selection][1] = input(
                "Please enter a part of the author's name: ")
            book = True

        elif (selection == 6):
            options[selection][1] = input(
                "Please enter a part of the publisher name: ")
            book = True
        elif (selection == 7):
            options[selection][1] = input(
                "Please enter the lower bound of the date of publication: ")
            options[selection][3] = input(
                "Please enter the upper bound of the date of publication: ")
            book = True
            
        elif (selection == 8):
            options[selection][1] = input(
                "Please enter the desired langauge code: ")
            book = True

        elif (selection == 9):
            options[selection][1] = input(
                "Please enter the lower bound of the average rating: ")
            options[selection][3] = input(
                "Please enter the upper bound of the average rating: ")
            book = True

    return bibNumber

# For viewing item details/status from the main menu


def view(database, cursor, userid):

    bibNumber = menu(database, cursor, False)
    
    if (bibNumber < 0):
        return userid

    sql = ("CREATE TEMPORARY TABLE tempInventory AS"
               + " (SELECT * FROM CheckoutRecords WHERE returnedDate IS NULL)")
    cursor.execute(sql)

    sql = "SELECT DISTINCT barcode FROM tempInventory WHERE bibNumber = " + str(bibNumber)
    cursor.execute(sql)
    barcodes = cursor.fetchall()
    print("\nBarcodes of units checked out for this item: ")
    for i in barcodes:
        print(i[0])

    sql = ("SELECT DISTINCT barcode FROM Inventory WHERE (barcode) NOT IN" +
            " (SELECT barcode FROM tempInventory) AND bibNumber = " + str(bibNumber))
    cursor.execute(sql)
    barcodes = cursor.fetchall()
    print("\nBarcodes of units in stock for this item: ")
    for i in barcodes:
        print(i[0])

    sql = "DROP TEMPORARY TABLE IF EXISTS tempInventory"
    cursor.execute(sql)

    # Get locations of items?
    sql = "select distinct description FROM Inventory INNER JOIN ItemCodes using(bibNumber) INNER JOIN LocationCodes using(code) INNER JOIN LibraryCodes using(code) where bibNumber = " + str(
        bibNumber)
    cursor.execute(sql)
    locations = cursor.fetchall()
    print("\nLocations where this item may be: ")
    for i in locations:
        print(i[0])

    sql = "SELECT bookID FROM Books WHERE bibNumber = " + str(bibNumber)
    cursor.execute(sql)
    result = cursor.fetchone()

    # Get ISBNs if book
    if (not result is None):
        bookID = result[0]

        sql = "SELECT ISBN FROM ISBN WHERE bookID = " + str(bookID)
        cursor.execute(sql)
        ISBN = cursor.fetchall()
        print("\nISBN numbers for this book: ")
        for i in ISBN:
            print(i[0])

    return userid
