import mysql.connector

import search
import checkout
import addRemove
import review
import mainMenu
import login

def logout(database, cursor):
    print("Logged out...")
    return -1 # User id for no user signed in

database = mysql.connector.connect(
  host="localhost",
  user="test",
  password="test",
  database="books"
)

cursor = database.cursor()

userid = -1

while (True):
    selection = mainMenu.menu()
    if (selection == "quit"):
        print("Exiting client... goodbye!")
        break
    else:
        userid = eval(selection)
    
