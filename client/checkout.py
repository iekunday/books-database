import mysql.connector
import login
import search
import datetime
import addRemove
import check

# Checks out an item for one month
def checkout(database, cursor, userID):

    barcode = input("\nPlease input the item's barcode: ")[0:13]
    
    bibNumber = addRemove.barcodeSearch(database, cursor, barcode)

    if (bibNumber < 0):
        return

    # Check if item is available
    sql = "SELECT * FROM CheckoutRecords WHERE returnedDate IS NULL AND barcode = " + barcode
    cursor.execute(sql)
    result = cursor.fetchone()

    if (result is None):
        sql = "INSERT INTO CheckoutRecords (bibNumber, barcode, userID, checkoutDate, dueDate) VALUES (%s, %s, %s, %s, %s)"
        val = (bibNumber, barcode, userID, datetime.datetime.today(), datetime.datetime.today() + datetime.timedelta(days=7))
        cursor.execute(sql, val)
        database.commit()
        print("Success!")
    else:
        print("Item is already checked out!")

# Returns an item
def returnItem(database, cursor, userID):

    barcode = input("\nPlease input the item's barcode: ")[0:13]
    
    sql = ("WITH X AS (SELECT * FROM CheckoutRecords WHERE barcode = %s)"
        + " UPDATE CheckoutRecords SET returnedDate = %s WHERE barcode = %s AND checkoutDate = (SELECT MAX(checkoutDate) FROM X)")
    val = (barcode, datetime.datetime.today(), barcode)
    cursor.execute(sql, val)
    database.commit()

# Prints the checkout records for UNRETURNED items
def record(database, cursor):
    sql = "SELECT firstName, lastName, barcode, title, checkoutDate, dueDate FROM Items INNER JOIN CheckoutRecords USING(bibNumber) INNER JOIN Users USING(userID) WHERE returnedDate IS NULL"
    cursor.execute(sql)
    result = cursor.fetchall()
    print(result)

# Pairs of menu options and the functions they correspond to
def menuOptions():
    return [
            ['Cancel', 'quit'],
            ['Checkout item', 'checkout(database, cursor, userid)'],
            ['Return item', 'returnItem(database, cursor,  userid)'],
            ['View unreturned checkout records', 'record(database, cursor)']
            ]

# The main user interface for the module
def menu(database, cursor, userid):

    while (True):

        if (userid < 0): 
            print("\nYou must be logged in to add/remove an item!")
            userid = login.prompt(database, cursor)[0]
            if (userid < 0):
                break

        print("\nWhat action do you want to perform? \n")

        i = 0
        for option in menuOptions():
            print(str(i) + " : " + option[0])
            i = i + 1

        selection = check.intInput("\nPlease enter your selection #: ", 0, 3, True)
        
        if (menuOptions()[selection][1] == "quit"):
            break
        else:
            eval(menuOptions()[selection][1])

    return userid
