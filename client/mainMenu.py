import check

def menuOptions():
    return [
            ['Quit', 'quit'],
            ['Browse items / Look up items', 'search.view(database, cursor, userid)'],
            ['Checkout/return item', 'checkout.menu(database, cursor, userid)'],
            ['Add/remove item(s) from database', 'addRemove.menu(database, cursor, userid)'],
            ['Leave book review', 'review.menu(database, cursor, userid)'],
            ['Login', 'login.prompt(database, cursor)'],
            ['Logout', 'logout(database, cursor)']
            ]

def menu():
    print("\nWelcome to the library client system!\nHere are your options:\n")
    options = menuOptions()

    i = 0
    for option in options:
        print(str(i) + " : " + option[0])
        i = i + 1

    selection = check.intInput("\nPlease enter your selection #: ", 0, 6, True)

    return(options[selection][1])

