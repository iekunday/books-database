# Enforces Yes/No answers to Yes/No prompts
def yesNo(prompt):
    while(True):
        selection = input(prompt)
        if (selection.lower() == 'y' or selection.lower() == 'n'):
            return selection
        print("Invalid input...")

# Enforces integer answers to prompts for integers
def intInput(prompt, minInt, maxInt, bounded):
    while(True):
        selection = input(prompt)
        try:
            selection = int(selection)
            if (selection < minInt):
                print("Invalid input...")
            elif (not bounded):
                return selection
            elif (selection > maxInt):
                print("Invalid input...")
            else:
                return selection
        except:
            print("Invalid input...")

