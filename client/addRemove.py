import mysql.connector
import login
import search
import check

# Searches for an author's name in the Authors table, inserts it if not present, and returns the authorID
def addAuthor(database, cursor, name):
    sql = "SELECT authorID FROM Authors WHERE name = %s"
    val = (name)
    cursor.execute(sql, val)
    result = cursor.fetchone()
    if (result is None):
        sql = "INSERT INTO Publishers (name) VALUES (%s)" 
        val = (name)
        cursor.execute(sql, val)
        database.commit()
        return cursor.lastrowid
    else:
        return result[0]

# Searches for a publisher's name in the Publishers table, inserts it if not present, and returns the publisherID
def addPublisher(database, cursor, name):
    sql = "SELECT publisherID FROM Publishers WHERE name = %s"
    val = (name)
    cursor.execute(sql, val)
    result = cursor.fetchone()
    if (result is None):
        sql = "INSERT INTO Publishers (name) VALUES (%s)" 
        val = (name)
        cursor.execute(sql, val)
        database.commit()
        return cursor.lastrowid
    else:
        return result[0]

# Adds a unit to the inventory of an existing item
def add(database, cursor):
    
    bibNumber = search.menu(database, cursor, False)
    barcode = input("\nPlease input the item's barcode: ")[0:13]

    sql = "INSERT INTO Inventory (bibNumber, barcode) VALUES (%s, %s)"
    val = (bibNumber, barcode)
    cursor.execute(sql, val)
    database.commit()

    # Update itemCount
    sql = "SELECT itemCount FROM Items WHERE bibNumber = " + str(bibNumber)
    cursor.execute(sql)
    result = cursor.fetchone()
    
    sql = "UPDATE Items SET itemCount = %s WHERE bibNumber = %s"
    val = (result[0] + 1, bibNumber)
    cursor.execute(sql, val)
    database.commit()

# Search for a barcode in the Inventory and return the corresponding bibNumber
def barcodeSearch(database, cursor, barcode):

    sql = "SELECT bibNumber FROM Items INNER JOIN Inventory USING(bibNumber) WHERE barcode = " + str(barcode)
    cursor.execute(sql)
    result = cursor.fetchone()
    
    if (result is None):
        print("Barcode not found!")
        return -1
    
    return result[0]

# Remove a single unit of an item (by its barcode) from the Inventory
def remove(database, cursor):

    barcode = input("\nPlease input the item's barcode: ")[0:13]
    
    bibNumber = barcodeSearch(database, cursor, barcode)

    sql = "DELETE FROM CheckoutRecords WHERE barcode = " + str(barcode)
    cursor.execute(sql)
    database.commit()

    sql = "DELETE FROM Inventory WHERE barcode = " + str(barcode)
    cursor.execute(sql)
    database.commit()

    # Update itemCount
    sql = "SELECT itemCount FROM Items WHERE bibNumber = " + str(bibNumber)
    cursor.execute(sql)
    result = cursor.fetchone()
    
    sql = "UPDATE Items SET itemCount = %s WHERE bibNumber = %s"
    val = (result[0] - 1, bibNumber)
    cursor.execute(sql, val)
    database.commit()

# Delete a book (by its bookID) from the Books table
def deleteBook(database, cursor, bibNumber):

    sql = "SELECT bookID FROM Books WHERE bibNumber = " + str(bibNumber)
    cursor.execute(sql)
    result = cursor.fetchone()

    if (result is None):
        return -1

    bookID = result[0]

    sql = "DELETE FROM ISBN WHERE bookID = " + str(bookID)
    cursor.execute(sql)
    database.commit()
    sql = "DELETE FROM userReviews WHERE bookID = " + str(bookID)
    cursor.execute(sql)
    database.commit()
    sql = "DELETE FROM BookRatings WHERE bookID = " + str(bookID)
    cursor.execute(sql)
    database.commit()
    sql = "DELETE FROM Publications WHERE bookID = " + str(bookID)
    cursor.execute(sql)
    database.commit()
    sql = "DELETE FROM BookAuthors WHERE bookID = " + str(bookID)
    cursor.execute(sql)
    database.commit()

    sql = "DELETE FROM Books WHERE bibNumber = " + str(bibNumber)
    cursor.execute(sql)
    database.commit()

    return bookID

# Remove an item (by its bibNumber) from the database entirely 
def removeAll(database, cursor):

    bibNumber = search.menu(database, cursor, False)

    deleteBook(database, cursor, bibNumber)
    
    sql = "DELETE FROM ItemCodes WHERE bibNumber = " + str(bibNumber)
    cursor.execute(sql)
    database.commit()
    sql = "DELETE FROM ItemCategories WHERE bibNumber = " + str(bibNumber)
    cursor.execute(sql)
    database.commit()
    sql = "DELETE FROM CheckoutRecords WHERE bibNumber = " + str(bibNumber)
    cursor.execute(sql)
    database.commit()
    sql = "DELETE FROM Inventory WHERE bibNumber = " + str(bibNumber)
    cursor.execute(sql)
    database.commit()
    
    sql = "DELETE FROM Items WHERE bibNumber = " + str(bibNumber)
    cursor.execute(sql)
    database.commit()
        

# Pairs of menu options and the functions they correspond to
def menuOptions():
    return [
            ['Cancel', 'quit'],
            ['Add unit of item', 'add(database, cursor)'],
            ['Remove unit of item', 'remove(database, cursor)']
            ]

# The main user interface for the module
def menu(database, cursor, userid):

    while (True):

        if (userid < 0): 
            print("\nYou must be logged in to add/remove an item!")
            userid = login.prompt(database, cursor)[0]
            if (userid < 0):
                break

        print("\nWhat action do you want to perform? \n")

        i = 0
        for option in menuOptions():
            print(str(i) + " : " + option[0])
            i = i + 1

        selection = check.intInput("\nPlease enter your selection #: ", 0, 2, True)
        
        if (menuOptions()[selection][1] == "quit"):
            break
        else:
            eval(menuOptions()[selection][1])

    return userid
