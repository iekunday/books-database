import mysql.connector
import check

# Register a new user and add them to the database
def register(database, cursor):
    firstName = input("Please enter your first name: ")[0:255]
    lastName = input("Please enter your last name: ")[0:255]

    sql = "INSERT INTO Users (firstName, lastName) VALUES (%s, %s)"
    val = (firstName, lastName)
    cursor.execute(sql, val)
    database.commit()
    userID = cursor.lastrowid

    print("\nWelcome " + firstName + " " + lastName + ". You have been registered.")
    print("Your user ID is " + str(userID) + ", you will use it to log in from now on.")

    return userID, firstName, lastName

# Login user from userID, without the need to log in again in the session
def login(database, cursor):

    userID = check.intInput("\nPlease enter your user ID: ", 0, 0, False)

    sql = "SELECT firstName, lastName FROM Users WHERE userID = " + str(userID)
    cursor.execute(sql)
    result = cursor.fetchone()

    firstName = "Null"
    lastName = "Void"
    if (result is None):
        print("User " + str(userID) + " does not exist, login failed.")
        userID = -1
    else:
        firstName = result[0]
        lastName = result[1]
        print("Welcome back " + firstName + " " + lastName + "! Login successful.") 

    return userID, firstName, lastName

# Prompt the user to login or register
def prompt(database, cursor):

    selection = check.yesNo("\nAre you an existing user? Y/N: ")
    if (selection.lower() == 'y'):
        return login(database, cursor)
    else:
        selection = check.yesNo("Do you want to register an account? Y/N: ")
        if (selection.lower() == 'y'):
            return register(database, cursor)

    print("\nCancelling login attempt...")        
    return -1, "Null", "Void"
