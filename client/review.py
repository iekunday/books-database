import mysql.connector
import login
import search
import check

# The text forms of the numeric rankings
def ratingOptions():
    return [
            ['Cancel',],
            ['"did not like it"'],
            ['"it was ok"'],
            ['"liked it"'],
            ['"really liked it"'],
            ['"it was amazing"']
            ]

# Leave review for a book
def leaveReview(database, cursor, bookID, userID):

    i = 0
    for option in ratingOptions():
        print(str(i) + " : " + option[0])
        i = i + 1
    
    rating = check.intInput("\nPlease enter your rating #: ", 0, 5, True)
    
    if (rating > 0):
        
        # Check if already reviewed
        sql = "SELECT rating FROM UserReviews WHERE bookID = " + str(bookID) + " AND userID = " + str(userID)
        cursor.execute(sql)
        oldRating = cursor.fetchone()

        if (oldRating is None):
            sql = "SELECT averageRating, totalRatings, ratingDist" + str(rating) + " FROM BookRatings WHERE bookID = " + str(bookID)
            cursor.execute(sql)
            result = cursor.fetchone()

            # Update the averageRating and rating distribution
            averageRating = (result[0] * result[1] + rating) / (result[1] + 1)
            sql = "UPDATE BookRatings SET averageRating = %s, totalRatings = %s, ratingDist%s = %s WHERE bookID = %s"
            val = (averageRating, result[1] + 1, rating, result[2] + 1, bookID)
            cursor.execute(sql, val)
            database.commit()

            # Add the review
            sql = "INSERT INTO UserReviews (userID, bookID, rating) VALUES (%s, %s, %s)" 
            val = (userID, bookID, rating)
            cursor.execute(sql, val)
            database.commit()

        else:
            sql = ("SELECT averageRating, totalRatings,"
                   + " ratingDist" + str(rating) + ", ratingDist" + str(oldRating[0])
                   + " FROM BookRatings WHERE bookID = " + str(bookID))
            cursor.execute(sql)
            result = cursor.fetchone()

            # Update the averageRating and rating distribtutions
            averageRating = (result[0] * result[1] + rating - oldRating[0]) / (result[1])
            sql = "UPDATE BookRatings SET averageRating = %s, totalRatings = %s, ratingDist%s = %s, ratingDist%s = %s WHERE bookID = %s"
            val = (averageRating, result[1], rating, result[2] + 1, oldRating[0], result[3] - 1, bookID)
            cursor.execute(sql, val)
            database.commit()

            # Update the review
            print("Book already rated! Over writing old rating...")
            sql = "UPDATE UserReviews SET rating = %s WHERE userID = %s AND bookID = %s" 
            val = (rating, userID, bookID)
            cursor.execute(sql, val)
            database.commit()


        print("Rating submitted!")

    else:
        print("Cancelled")
        
# Identify book by ISBN, return bookID
def isbnSearch(database, cursor):

    ISBN = input("\nPlease input the book's ISBN: ")

    sql = "SELECT bookID FROM ISBN WHERE isbn = " + ISBN
    cursor.execute(sql)
    result = cursor.fetchone()
    
    if (result is None):
        print("ISBN not found!")
        return -1
    
    return result[0]

# Search for book using general search, return bookID
def fullSearch(database, cursor):

    bibNumber = search.menu(database, cursor, True)

    sql = "SELECT bookID FROM Books WHERE bibNumber = " + str(bibNumber)
    cursor.execute(sql)
    result = cursor.fetchone()
    
    if (result is None):
        print("Item not found or not a book!")
        return -1
    
    return result[0]

# Pairs of menu options and the functions they correspond to
def menuOptions():
    return [
            ['Cancel', 'quit'],
            ['Yes', 'isbnSearch(database, cursor)'],
            ['No', 'fullSearch(database, cursor)']
            ]

# The main user interface for the module
def menu(database, cursor, userID):

    while (True):

        if (userID < 0): 
            print("\nYou must be logged in to leave a review!")
            userID = login.prompt(database, cursor)[0]
            if (userID < 0):
                break

        print("\nDo you know the ISBN of the book that you want to review?: \n")

        i = 0
        for option in menuOptions():
            print(str(i) + " : " + option[0])
            i = i + 1

        selection = check.intInput("\nPlease enter your selection #: ", 0, 2, True)
        
        if (menuOptions()[selection][1] == "quit"):
            break
        else:
            bookID = eval(menuOptions()[selection][1])
            if (bookID >= 0):
                leaveReview(database, cursor, bookID, userID)
            selection = check.yesNo("\nDo you want to review another book? Y/N: ")
            if (not selection.lower() == 'y'):
                break

    return userID
