# Books Database

_Default Datasets Needed_

- book1-100k.csv
- Checkouts_By_Title_Data_Lens_2017.csv
- Integrated_Library_System\_\_ILS\_\_Data_Dictionary.csv
- Library_Collection_Inventory.csv
- user_rating_0_to_1000.csv

_Generated Datasets Needed_

- Books_ISBN.csv
- Item_Categories.csv
- User_Checkouts.csv
- User_Reviews.csv

_Server setup_

1. OPTIONAL: Run the split scripts first to generate new CSVs
2. OPTIONAL: Run the user-rating-generation script
3. Create a new database and connect to it
4. Run the create-database script
5. Run the load-data script

If you choose to run the server locally, the path names will need to be updated in load-data.sql

_Client Setup_

1. Create new virtual environment `python3 -m venv books`
2. In your shell, run `source books/bin/activate`
3. Install mysql dependency `pip3 install mysql-connector-python`
4. Update your server credentials in `client/client.py`
5. Start client `python3 client/client.py`
